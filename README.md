# Yotta Project 2 - Deep Learning - Marmitoph

Copyright : 2020, Marie Verdoux, Alessio Rea, Jerome Assouline

==============================

# Getting started

## 0. Clone this repository
==============================

```
$ git clone https://gitlab.com/yotta-academy/mle-bootcamp/projects/dl-projects/fall-2020/projet_marmitoph.git
$ cd projet_marmitoph
```

## 1. Setup your virtual environment and activate it

Goal : create a local virtual environment in the folder `./.venv/`.

- First: check your python3 version:

    ```
    $ python3 --version
    # examples of outputs:
    Python 3.6.2 :: Anaconda, Inc.
    Python 3.7.2

    $ which python3
    /Users/benjamin/anaconda3/bin/python3
    /usr/bin/python3
    ```

    - If you don't have python3 and you are working on your mac: install it from [python.org](https://www.python.org/downloads/)
    - If you don't have python3 and are working on an ubuntu-like system: install from package manager:

        ```
        $ apt-get update
        $ apt-get -y install python3 python3-pip python3-venv
        ```

- Now that python3 is installed create your environment, activate it and install necessary packages:

    ```
    $ source init.sh
    ```

    You sould **allways** activate your environment when working on the project.

    If it fails with one of the following message :
    ```
    "ERROR: failed to create the .venv : do it yourself!"
    ```

## 2. Launch application !

Execute the following command :

    ```
    $ source launch_marmitoph.sh
    ```

## 3. Click on link 

You can now view your Streamlit app in your browser.

Click or Copy-Paste on your local URL


# Project Organization (TODO : update structure)

Yotta Project 2 - Deep Learning - From photo to recipe

Project Organization
------------
```
.
├── activate.sh
├── assignment
│   └── Project 2 - Computer Vision NLP.pdf
├── data
├── init.sh
├── install_packages.sh
├── launch_marmitoph.sh
├── LICENSE
├── Makefile
├── models
│   ├── label_dict.pickle
│   └── model_finetuned.h5
├── notebooks
│   ├── explo_API_marmiton.ipynb
│   ├── export_openfoodfacts_data.ipynb
│   ├── food_classification_train_model.ipynb
│   ├── model_evaluation.ipynb
│   ├── preprocess_openfoodfacts_data.ipynb
│   └── script_train_with_tfdata.ipynb
├── README.md
├── requirements_dev.txt
├── requirements.txt
├── setup.py
├── src
│   ├── application
│   │   ├── example_photo.jpg
│   │   ├── __init__.py
│   │   └── marmitoph_app.py
│   ├── domain
│   │   ├── __init__.py
│   │   ├── recipes.py
│   │   ├── streamlit_app_functions.py
│   │   └── streamlit_classes.py
│   ├── infrastructure
│   │   ├── export_openfoodfacts_data.py
│   │   └── __init__.py
│   ├── __init__.py
│   ├── interface
│   │   ├── computer_vision_lib.py
│   │   ├── __init__.py
│   │   └── pantry.jpg
│   └── settings
│       ├── __init__.py
│       └── settings.py
└── tox.ini
```



--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
